package com.crealytics.webapp.models;
import java.text.DecimalFormat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportResponse {
  
  String month;
  String site;
  String requests;
  String impressions;
  String clicks;
  String conversions;
  String revenue;
  String CTR;
  String CR;
  String fill_rate;
  String eCPM;
  
  public ReportResponse(String month, String site, String requests,String impressions,String clicks,String conversions,String revenue){
	DecimalFormat df = new DecimalFormat();
    df.setMaximumFractionDigits(2);
		
	this.month = month;
    this.site = site;
    this.requests = requests;
    this.impressions = impressions;
    this.clicks = clicks;
    this.conversions= conversions;
    this.revenue= revenue;
    this.CTR = df.format((Float.parseFloat(clicks)/Float.parseFloat(impressions))*100);
    CR = df.format((Float.parseFloat(conversions)/Float.parseFloat(impressions))*100);
    fill_rate = df.format((Float.parseFloat(impressions)/Float.parseFloat(requests))*100);
    eCPM = df.format((Float.parseFloat(revenue)*1000)/Float.parseFloat(impressions));
  }
  
  public void setMonth(String month) {
	this.month = month;
  }
  
  public void setSite(String site) {
	this.site = site;
  }

  @JsonProperty
  public String getMonth() {
    return month;
  }
  @JsonProperty
  public String getSite() {
    return site;
  }
  @JsonProperty
  public String getRequests() {
    return requests;
  }
  @JsonProperty
  public String getImpressions() {
    return impressions;
  }
  @JsonProperty
  public String getClicks() {
    return clicks;
  }
  @JsonProperty
  public String getConversions() {
    return conversions;
  }
  @JsonProperty
  public String getRevenue() {
    return revenue;
  }
  @JsonProperty
  public String getCTR() {
    return CTR;
  }
  @JsonProperty
  public String getCR() {
    return CR;
  }
  @JsonProperty
  public String getFill_rate() {
    return fill_rate;
  }
  @JsonProperty
  public String geteCPM() {
    return eCPM;
  }
}
