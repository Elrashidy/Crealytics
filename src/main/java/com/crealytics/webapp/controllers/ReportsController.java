package com.crealytics.webapp.controllers;

import java.text.ParseException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crealytics.webapp.helpers.ReportsHelper;
import com.crealytics.webapp.models.ReportResponse;

@RestController
public class ReportsController {

    @RequestMapping(value="/reports",produces={"application/json"}, method = RequestMethod.GET)
    public ReportResponse reports(
        @RequestParam(value="month", required=false)  String month,
        @RequestParam(value="site", required=false) String site
    ) throws ParseException{
    	return ReportsHelper.getReportResponse(month,site);
    }
}
