package com.crealytics.webapp.helpers;

import com.crealytics.webapp.models.ReportResponse;
import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CSVUtils {

	public static ArrayList<ReportResponse> parseCSVFile(String filePath) {
		ArrayList<ReportResponse> advertisingDataList = new ArrayList<ReportResponse>();
		FileReader file = null;
		try {
			file = new FileReader(filePath);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		if (file != null) {
			CSVReader reader;
			try {
				String monthFromFilePath = filePath.substring(filePath.lastIndexOf('_') - 2, filePath.lastIndexOf('_'));
				int monthNum = Integer.parseInt(monthFromFilePath) - 1;
				String month = ReportsHelper.getMonthForInt(monthNum);
				reader = new CSVReader(file);
				String[] line = reader.readNext(); // skip the header line
				while ((line = reader.readNext()) != null) {
					if (line.length == 6) {
						advertisingDataList.add(new ReportResponse(month, line[0], line[1].trim(),
								line[2].trim(), line[3].trim(),
								line[4].trim(), line[5].trim()));
					}
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return advertisingDataList;
	}
}
