package com.crealytics.webapp.helpers;

import java.io.File;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.stream.Collectors;

import com.crealytics.webapp.models.ReportResponse;


public class ReportsHelper {

	private static  ArrayList<ReportResponse> getReportsDataFromCSV(String month, String site){
    	String resoucesPath = new File("").getAbsolutePath()+"/src/main/assets/reportsCSV/";
        String csvFile1Path = resoucesPath+"2018_02_report.csv";
        String csvFile2Path = resoucesPath+"2018_01_report.csv";
        ArrayList<ReportResponse> advertisingDataList = new ArrayList<ReportResponse>();
        advertisingDataList.addAll(CSVUtils.parseCSVFile(csvFile1Path));
        advertisingDataList.addAll(CSVUtils.parseCSVFile(csvFile2Path));
        return advertisingDataList;
	}


	public static ReportResponse getReportResponse(String month, String site) throws ParseException {
		if(month!=null){
			month = updateMonthFormat(month);
		}
		final String formattedMonth = month;
		ArrayList<ReportResponse> reportsDataFromCSV = getReportsDataFromCSV(formattedMonth, site);
		if (formattedMonth == null && site == null)
			return SumAggregation(reportsDataFromCSV, formattedMonth, site);
		else if (formattedMonth == null)
			return SumAggregation((ArrayList<ReportResponse>) reportsDataFromCSV.stream().filter(record -> record.getSite().equalsIgnoreCase(site)).collect(Collectors.toList()), formattedMonth, site);
		else if (site == null)
			return SumAggregation((ArrayList<ReportResponse>) reportsDataFromCSV.stream().filter(record -> record.getMonth().equalsIgnoreCase(formattedMonth)).collect(Collectors.toList()), formattedMonth, site);
		else {
			for(ReportResponse advertisingData: reportsDataFromCSV){
				if(advertisingData.getMonth()!=null && advertisingData.getMonth().toLowerCase().equalsIgnoreCase(formattedMonth.toLowerCase())
						&& advertisingData.getSite()!=null && advertisingData.getSite().equalsIgnoreCase(site)){
					return advertisingData;
				}
			}
		}
		return null;
	}
	
	public static ReportResponse SumAggregation(ArrayList<ReportResponse> AdvertisingDataList, String month, String site) {
		int requestsSum = 0;
		int impressionsSum = 0;
		int clicksSum = 0;
		int conversionsSum = 0;
		float revenueSum = 0;
		for (ReportResponse record : AdvertisingDataList) {
			requestsSum += Integer.parseInt(record.getRequests());
			impressionsSum += Integer.parseInt(record.getImpressions());
			clicksSum += Integer.parseInt(record.getClicks());
			conversionsSum += Integer.parseInt(record.getConversions());
			revenueSum += Float.parseFloat(record.getRevenue());
		}
		ReportResponse response = new ReportResponse(null, null, requestsSum+"",
				impressionsSum+"", clicksSum+"",
				conversionsSum+"", revenueSum+"");
		if (month != null) {
			response.setMonth(month);
		}
		if (site != null)  {
			response.setSite(site);;
		}
		return response;
	}
	
	private static String updateMonthFormat(String month) throws ParseException {
	    month=getMonthForInt(getMonthFormat(month));
	    if(month==null){
			throw new ParseException("Invalid Month Format",0);
		}
	    return month;
	}

	private static int getMonthFormat(String month) {
		if(month==null){
			return -1;
		}
		int monthNum;
		//check if month name
		SimpleDateFormat sdf = new SimpleDateFormat("MMM");
	    try {
			monthNum = sdf.parse(month).getMonth();
		} catch (ParseException e) {
			try {
				sdf = new SimpleDateFormat("MMMM");
				monthNum = sdf.parse(month).getMonth();
			} catch (ParseException e1) {
				monthNum = -1;
			}
		}
	    if(monthNum!=-1 && isValidMonthName(month)){
	    	return monthNum;
	    }
	    
		//check if month number
		sdf = new SimpleDateFormat("MM");
	    try {
	    	if (Integer.parseInt(month) > 0 && Integer.parseInt(month) < 13)
	    		monthNum = sdf.parse(month).getMonth();
		} catch (ParseException e) {
			return -1;
		}
	    return monthNum;
	}
	
	 private static boolean isValidMonthName(String month) {
		 DateFormatSymbols dfs = new DateFormatSymbols();
		 	// check long month names
	        String[] months = dfs.getMonths();
			for (String monthName : months) {
	            if(month.equalsIgnoreCase(monthName)){ 
	            	return true;
	            }
	        }
		 	// check short month names
	        months = dfs.getShortMonths();
			for (String monthName : months) {
	            if(month.equalsIgnoreCase(monthName)){
	            	return true;
	            }
	        }
	         return false;
	}
	
	public static String getMonthForInt(int num) {
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            return months[num];
        }
        return null;
    }

}
