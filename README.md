# Crealytics Challenge

It is RESTful web service that can offer reporting advertising data, part of which
is extracted from a csv file and the rest being calculated as additional metrics.

## Technologies Used
- Spring Boot 2.0.2
- Maven
- JDK 8
- Eclipse Neon 3
- Tomcat 8.5

## Structure, it contains 4 packages
  - Application, which contains the `Application.java` file
  - controllers, which contains `ReportsController.java` (has the API)
  - models, which contains the response model
  - helpers, which parse input params, read and parse CSV file and construct the response

## Instructions
- install used technologies
- runs `Application.java` as application, then visit `http://localhost:8080/reports?month=123`
  
## API
  it is `GET` API which expects input month and site like that `/reports?month=january&site=iOS`
  and returns json response like that
  ``` 
  {
	  "month":"January",
	  "site":"iOS",
	  "requests":"2550165",
	  "impressions":"2419733",
	  "clicks":"6331",
	  "conversions":"1564",
	  "revenue":"4692.28",
	  "fill_rate":"94.89",
	  "eCPM":"1.94",
	  "ctr":"0.26",
	  "cr":"0.06"
  }
  ```
  
  It expects month in different formats, all next requests are valid and return the same response:
  - full_name `/reports?month=january&site=iOS`
  - case_insensitive `/reports?month=January&site=iOS`
  - numbers `/reports?month=1&site=iOS` and `/reports?month=01&site=iOS`
  - short_name `/reports?month=jan&site=iOS`
  
  It returns `Invalid Month Format` in case month was not one of the above like `/reports?month=bad&site=iOS`

  It also makes `sum` aggregation in case that site or month are missing (or both):
  - `/reports?month=january` returns
    
    ```
    {
        "month":"January",
        "requests":"34853988",
        "impressions":"33100001",
        "clicks":"86982",
        "conversions":"21406",
        "revenue":"65411.76",
        "fill_rate":"94.97",
        "eCPM":"1.98",
        "ctr":"0.26",
        "cr":"0.06"
    }
    ```
    
  - `/reports?site=android` returns:
    
    ```
    {
        "site":"android",
        "requests":"18835321",
        "impressions":"17755397",
        "clicks":"47329",
        "conversions":"11365",
        "revenue":"35320.53",
        "fill_rate":"94.27",
        "eCPM":"1.99",
        "ctr":"0.27",
        "cr":"0.06"
    }
    ```
    
  - `/reports?site=android` returns:
    
    ```
    {
        "requests":"68823820",
        "impressions":"64422713",
        "clicks":"184724",
        "conversions":"39477",
        "revenue":"128351.91",
        "fill_rate":"93.61",
        "eCPM":"1.99",
        "ctr":"0.29",
        "cr":"0.06"
    }
    ```
    
## Testing
  Still missing, it should at least:
  - test `ReportResponse.java` for creation, getters, setters and generating the metrics
  - test CSV reading and parsing
  - integration test for API (maybe mocking CSV data for more special cases):
    - it should return 200 in normal scenario
    - it should return 500 in case of wrong month
    - it should return empty response in case of no data available
    - it should test different month formats
    - it should test aggregation in case of missing month or site
